// @flow
import {createRouter} from '@exponent/ex-navigation';
import {FocusComponent, SettingsComponent, PlanListComponent} from "./components";
import { Examples } from '@shoutem/ui';



export const Router = createRouter(() => ({
    timer: () => FocusComponent,
    settings: () => SettingsComponent,
    planList: ()=> PlanListComponent,
    shoutemExample: () => Examples

}));
