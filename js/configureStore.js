// @flow
import {AsyncStorage} from 'react-native';
import devTools from 'remote-redux-devtools';
import {createStore, applyMiddleware, compose} from 'redux';
import {persistStore, autoRehydrate} from 'redux-persist';
import {appReducer} from './reducers';
import createLogger from 'redux-logger';
import {createEpicMiddleware, combineEpics} from "redux-observable";
import {appEpics} from "./reducers/index";
import {createNavigationEnabledStore, NavigationReducer} from '@exponent/ex-navigation';


import {TimerActionNames} from "./reducers/timer.actions";
import {initialState} from "./initialState";
// import { enableBatching } from 'redux-batched-actions';

export default function configureStore(onCompletion: ()=>void): any {

    const epicMiddleware = createEpicMiddleware(appEpics);

    const enhancer = compose(
        applyMiddleware(epicMiddleware/*, createLogger({
         diff: true,
         predicate: (getState, action) => action.type !== TimerActionNames.FOCUS_TICK

         })*/),
        devTools({
            name: 'focuspol', realtime: true,
            actionsBlacklist:['timer/TICK']
        }),
    );

    // const storeWithNavigation = createNavigationEnabledStore({
    //     createStore,
    //     navigationStateKey: 'navigation'
    // })(appReducer, enhancer, autoRehydrate());
    // persistStore(storeWithNavigation, { storage: AsyncStorage, keyPrefix:'focuspolPersist' }, onCompletion);

    const storeWithNavigation = createNavigationEnabledStore({
        createStore,
        navigationStateKey: 'navigation'
    })(appReducer, initialState, enhancer);

    setTimeout(() => {
        onCompletion();
    }, 10);

    return storeWithNavigation;
}


