// @flow
import R from 'ramda';
import {Occurrence, OccurrenceType} from "../models/occurrence";
import {MomentUtils} from "../utils/moment-utils";
import {PushNotificationIOS} from "react-native";
import moment from 'moment';


export class LocalNotificationsManager {

    static cancelAll = () => {
        // console.log('cancel all notifications');
        PushNotificationIOS.cancelAllLocalNotifications();
    }

    static createAllLocalNotifications = (todayItemsLeft : Occurrence[]) => {
        if (!todayItemsLeft || todayItemsLeft.length == 0)
            return;

        //console.log('createAllLocalNotifications');
        let executeCreateNotification = occ => {
            LocalNotificationsManager.createLocalNotification(occ);
        };

        R.forEach(executeCreateNotification, R.drop(1, todayItemsLeft));

        LocalNotificationsManager.createFinishedLocalNotification(moment(R.last(todayItemsLeft).endDate));
    }

    static createNextNotification = (todayItemsLeft : Occurrence[]) => {
        if (!todayItemsLeft || todayItemsLeft.length == 0)
            return;

        let occ = R.head(todayItemsLeft);
        LocalNotificationsManager.createLocalNotification(occ);
    }

    static createLocalNotification = (occurrence: Occurrence) => {

        const formattedDuration = MomentUtils.formattedDuration(Occurrence.getDuration(occurrence));
        const formattedEndDate = MomentUtils.formattedTime(moment(occurrence.endDate));
        let body = R.cond([
            [R.equals(OccurrenceType.focus), R.always(`work now for ${formattedDuration} until ${formattedEndDate}`)],
            [R.T, () => `take ${formattedDuration} break until ${formattedEndDate}`]
        ])(occurrence.type);

        const config =
            {

                fireDate: MomentUtils.formatNDate(occurrence.startDate),
                // fireDate: MomentUtils.formatNDate(moment()),
                alertBody: body,
                sound: 'default',
                badge: '+1',
                category: 'focuspol'
            };

        PushNotificationIOS.scheduleLocalNotification(config);
    }

    static createFinishedLocalNotification = (endDate : moment$Moment) => {
        const config =
            {

                fireDate: MomentUtils.formatNDate(endDate),
                // fireDate: MomentUtils.formatNDate(moment()),
                alertBody: `Congratulations you've finished all pomodoros for today!`,
                sound: 'default',
                badge: '+1',
                category: 'focuspol'
            };

        PushNotificationIOS.scheduleLocalNotification(config);

    }
}
