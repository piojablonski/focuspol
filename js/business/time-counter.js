// @flow

import Rx from 'rxjs';
import moment from 'moment';


class TimeCounter {
    roundToValue = 0;

    timer = Rx.Observable.timer(0, 200);
    pauser = new Rx.BehaviorSubject(true);

    pausable = this.pauser.switchMap(paused => {
            //console.log(`pausable ${paused}`);

            return paused ? Rx.Observable.never() :
                this.timer.map(index => {
                    const now = moment();
                    if (!this.roundToValue)
                        this.roundToValue = now.milliseconds();

                    let result = now.milliseconds() < this.roundToValue ? now.subtract(1, 's').startOf('s') : now.startOf('s');
                    return result;
                });

        }
    );

    pause = () => {
        if (this.pauser.getValue() == false) {
            this.pauser.next(true);
        }
    };

    play = () => {
        if (this.pauser.getValue() == true) {
            this.pauser.next(false);
        }
    }
}

export const timeCounter = new TimeCounter();
