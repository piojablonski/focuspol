// @flow
import Sound from 'react-native-sound';

class SoundService {
    alarmSound : Sound;
    constructor() {
        this.alarmSound = new Sound('alarm_1_beep.wav', Sound.MAIN_BUNDLE, error=>{
            if (error) {
                console.log('failed to load the sound', error);
            } else {

            }
        });

    };

    playFocusAlarm = () => {

        this.alarmSound.setNumberOfLoops(0);
        this.alarmSound.play();
    };

    playBreakAlarm = () => {
      //  this.alarmSound.setNumberOfLoops(1);
       // this.alarmSound.play();
    };

    stop = () => {
        this.alarmSound.stop();
    };


};

export const soundService = new SoundService();