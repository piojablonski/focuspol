// @flow
import type {SettingsState, TimerState} from '../models';
import {Occurrence, OccurrenceType} from '../models';
import R from 'ramda';
import moment from 'moment';
import {OccurrenceStatus} from "../models/occurrence";
import {generateUUID} from "../utils/uuid";

export class TimerHelper {


    static getNextOccurrenceIndex(todayItems: Occurrence[], newOccurrenceType: string): number {
        let result = "";
        if (newOccurrenceType == OccurrenceType.focus) {
            let lastOccurrenceIndex = R.prop('index')(R.findLast(R.propEq('type', OccurrenceType.focus))(todayItems));
            result = lastOccurrenceIndex ? lastOccurrenceIndex + 1 : 1;
        }
        return result;
    }


    static getNextOccurrenceType = (settings: SettingsState, todayItems: Occurrence[]): string => {
        let newType: string;

        const finishedCount = todayItems ? todayItems.length : 0;
        if (finishedCount == 0) {
            newType = OccurrenceType.focus;
        } else {
            let lastHistoryItem = R.last(todayItems);

            if (lastHistoryItem.type == OccurrenceType.shortBreak || lastHistoryItem.type == OccurrenceType.longBreak) {
                newType = OccurrenceType.focus
            }
            else {
                let shouldBeLongBreak =
                    R.pipe(
                        R.takeLastWhile(ht => ht.type != OccurrenceType.longBreak),
                        R.countBy(R.prop('type')),
                        R.propEq(OccurrenceType.focus, settings.longBreakAfter)
                    )(todayItems);


                if (shouldBeLongBreak)
                    newType = OccurrenceType.longBreak;
                else {
                    newType = OccurrenceType.shortBreak;
                }
            }

        }

        return newType;

    }


    static generateNextOccurrence = (settings: SettingsState, todayItems: Occurrence[], now: moment$Moment, startNow = true): Occurrence => {
        const newType = TimerHelper.getNextOccurrenceType(settings, todayItems);
        //console.log('getNextOccurrence now', now);
        let startDate;
        if (startNow || todayItems.length == 0) {
            startDate = now.toJSON()
        }
        else {
            startDate =
                R.pipe(
                    R.last(),
                    R.prop('endDate')
                )(todayItems)
        }
        let timerDuration = Occurrence.durationByType(newType, settings);
        //const startDate = startNow ? moment().toJSON() :

        const newOccurrence: Occurrence = R.evolve(
            {
                id: () => generateUUID(),
                startDate: () => moment(startDate).toJSON(),
                endDate: () => moment(startDate).add(timerDuration).toJSON(),
                type: () => newType,
                status: () => OccurrenceStatus.queue
            }
        )(Occurrence.createEmptyOccurrence());

        return newOccurrence;
    }


    static updateExecutionPlan = (settings: SettingsState, todayHistoryItems: Occurrence[], currentExecutionPlan: Occurrence[], now: moment$Moment): Occurrence[] => {
        if (!currentExecutionPlan || currentExecutionPlan.length == 0) {
            return TimerHelper.createExecutionPlan(settings, todayHistoryItems, now);
        } else {
            return TimerHelper.refreshExecutionPlanStartDate(currentExecutionPlan, now);
        }
    }

    static refreshExecutionPlanStartDate = (executionPlan: Occurrence[], now: moment$Moment): Occurrence[] => {
        const newStartDate = now;
        const oldStartDate = moment(executionPlan[0].startDate);

        const diff = newStartDate.diff(oldStartDate);

        let refreshDates = (occ: Occurrence) => {
            return R.merge(
                occ
            )({
                startDate: moment(occ.startDate).add(diff).toJSON(),
                endDate: moment(occ.endDate).add(diff).toJSON()
            });
        }

        const newExecutionPlan = R.map(refreshDates, executionPlan);


        return newExecutionPlan;
    }

    static createExecutionPlan = (settings: SettingsState, todayItems: Occurrence[], now: moment$Moment): Occurrence[] => {

        let isFocus = (o: Occurrence) => o.type == OccurrenceType.focus;

        let plan: Occurrence[] = [];

        let items = R.clone(todayItems);

        let focusItems = [];

        let i = 0;
        while (focusItems.length < settings.target) {
            let newOccurrence = TimerHelper.generateNextOccurrence(settings, items, now, i++ == 0);
            let index = TimerHelper.getNextOccurrenceIndex(items, newOccurrence.type);
            newOccurrence.index = index;
            items = R.append(newOccurrence, items);
            focusItems = R.filter(occ => occ.type == OccurrenceType.focus, items);
            plan = R.append(newOccurrence, plan);

        }

        return plan;

    }
}
