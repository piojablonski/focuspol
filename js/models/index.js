// @flow

import moment from 'moment';
import {Occurrence, OccurrenceType} from './occurrence';

export {
    Occurrence,
    OccurrenceType
};

export type TimerState = {
    isRunning : boolean,
    isNotificationSet : boolean,
    currentTimerValue : string,
    executionPlan : Occurrence[],
}

export type HistoryItem = {
    startDate? : string,
    endDate? : string,
    // expectedDuration? : string,
    // currentDuration? : string,
    type? : string,
}

export type HistoryState = {
    items : Occurrence[]
}

export type SettingsState = {
    username: string,
    autoplay : boolean,
    workDuration: string,
    shortBreakDuration : string,
    longBreakDuration : string,
    longBreakAfter: number,
    target : number
}

export type FocuspolState = {
    settings : SettingsState,
    timer : TimerState,
    history : HistoryState
}

export type TimerComponentDisplayStatus = 'occurrenceCompleted' | 'planCompleted' | 'timer' | 'beforeStart';

export type TimerComponentSelectedState = {
    isOccurrenceCompleted : boolean,
    isPlanCompleted : boolean,
    shouldCreateNotifications : boolean,
    shouldClearNotifications : boolean,
    shouldPlayAlarm :boolean,
    shouldDisplay : TimerComponentDisplayStatus
}

export type ExecutionPlanStatistics = {
    focusLeftToday : number,
    focusFinishedToday : number
}
