// @flow
import moment from 'moment';
import type {SettingsState} from './index'
import R from 'ramda';

export const OccurrenceType = {
    focus: 'focus',
    shortBreak: 'shortBreak',
    longBreak: 'longBreak',
};

export const OccurrenceStatus = {
    queue: 'queue',
    finished: 'finished'
};

export class Occurrence {
    id: string;
    startDate: string;
    endDate: string;
    type: string;
    status: string;
    index: number;

    static durationByType = (type: string, settings: SettingsState): moment$MomentDuration => {
        return ({
            [OccurrenceType.focus]: moment.duration(settings.workDuration, 'm'),
            [OccurrenceType.shortBreak]: moment.duration(settings.shortBreakDuration, 'm'),
            [OccurrenceType.longBreak]: moment.duration(settings.longBreakDuration, 'm'),
        })[type || 0];

    }

    static getDuration = (occ : Occurrence) : moment$MomentDuration => {
        return moment.duration(moment(occ.endDate).diff(moment(occ.startDate)));
    }

    static createEmptyOccurrence(): Occurrence {
        return {
            id: "",
            type: "",
            startDate: "",
            endDate: "",
            status:""
        };
    };


}
