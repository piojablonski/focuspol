// @flow
import R from 'ramda';
import {Occurrence} from "../models/occurrence";
import {TimerHelper} from '../business/timer-helper';
import {getTimerComponentSelectedState, getCompletedExecutionPlanOccurrences} from "../selectors";
import {historyActions} from "./history.actions";
import {timeCounter} from "../business/time-counter";
import type {SettingsState, HistoryItem, TimerState, FocuspolState, TimerComponentSelectedState} from '../models';
import {LocalNotificationsManager} from "../business/local-notifications-manager";

import * as Rx from 'rxjs';
import {soundService} from "../business/sound-service";
import {persistStore} from 'redux-persist';
import {AsyncStorage} from 'react-native';

export const TimerActionNames = {
    FOCUS_PLAY: 'timer/PLAY',
    FOCUS_RESUME: 'timer/RESUME',
    FOCUS_STOP: 'timer/STOP',
    FOCUS_STOPPED: 'timer/STOPPED',
    FOCUS_TICK: 'timer/TICK',
    FOCUS_SET_NEXT: 'timer/SET_NEXT',
    FOCUS_SET_NOTIFICATION: 'timer/SET_NOTIFICATION',
    FOCUS_UNSET_NOTIFICATION: 'timer/UNSET_NOTIFICATION',
    NEXT_OCCURRENCE: 'timer/NEXT_OCCURRENCE',
    FOO: 'timer/FOO',
    BAR: 'timer/BAR',
    BAZ: 'timer/BAZ',
    NONE: 'timer/NONE',
};

export const timerEpics = [



    (action$: any, store: any) => action$.ofType(TimerActionNames.FOCUS_RESUME)
        .do(timeCounter.play)
        .combineLatest(timeCounter.pausable)
        .mergeMap(a => {
            //console.log(a[1]);
            let [x ,now]  =a;
            let state: FocuspolState = store.getState();
           // console.log('pausable executed', state.timer.isRunning);

            if (!state.timer.isRunning) {
                return Rx.Observable.of(timerActions.tick(now));
            } else {


                let selectedState: TimerComponentSelectedState = getTimerComponentSelectedState(state);

                if (selectedState.isOccurrenceCompleted) {
                    let completedOccurrences = getCompletedExecutionPlanOccurrences(state);
                    let actionToDispatch = [
                        Rx.Observable.of(historyActions.saveFocus(completedOccurrences)),
                        Rx.Observable.of(timerActions.nextOccurrence(completedOccurrences)),
                        Rx.Observable.of(timerActions.tick(now))
                    ];
                    return Rx.Observable.concat(...actionToDispatch);
                } else if (selectedState.isPlanCompleted) {
                    return Rx.Observable.of(timerActions.stop());
                }
                else {
                    return Rx.Observable.of(timerActions.tick(now));
                }
            }
        }),

    // (action$: any) => action$.ofType(TimerActionNames.FOO)
    //     .mergeMap(a => {
    //         return Rx.Observable.concat(
    //             Rx.Observable.of(timerActions.bar()),
    //             Rx.Observable.of(timerActions.baz())
    //         );
    //     }),

    (action$: any, store: any) => action$.ofType(TimerActionNames.FOCUS_STOP)
        //.do(timeCounter.pause)
        .mergeMap(a => {
            let actionToDispatch = [
                Rx.Observable.of(timerActions.stopped())
            ];
            const state: FocuspolState = store.getState();
            if (state.timer.isNotificationSet) {
                LocalNotificationsManager.cancelAll();
                actionToDispatch = actionToDispatch.concat(Rx.Observable.of(timerActions.unsetNotification()))
            }
            return Rx.Observable.concat(...actionToDispatch);
        }),

    (action$: any, store: any) => action$.ofType(TimerActionNames.FOCUS_PLAY)
        .mergeMap(a => {
            let actionToDispatch = [/*Rx.Observable.of(timerActions.resume())*/];
            actionToDispatch = actionToDispatch.concat(Rx.Observable.of(timerActions.setNotification()))
            const state: FocuspolState = store.getState();
            if (!state.timer.isNotificationSet) {
                LocalNotificationsManager.createAllLocalNotifications(state.timer.executionPlan);
            }
            return Rx.Observable.concat(...actionToDispatch);
        }),

    // (action$: any, store: any) => action$.ofType(TimerActionNames.FOCUS_PLAY)
    //     .do(()=>{
    //         console.log('focus_play');
    //     })
    //     .mapTo({})


];

export const timerActions = {

    play: (settings: SettingsState, todayHistoryItems: Occurrence[], currentExecutionPlan: Occurrence[], now : moment$Moment) => {
        let executionPlan = TimerHelper.updateExecutionPlan(settings, todayHistoryItems, currentExecutionPlan, now);
        return {
            type: TimerActionNames.FOCUS_PLAY,
            executionPlan: executionPlan
        }
    },

    resume: () => {
        return {
            type: TimerActionNames.FOCUS_RESUME,
        }
    },

    stop: () => {
        return {
            type: TimerActionNames.FOCUS_STOP
        }
    },
    stopped: () => {
        return {
            type: TimerActionNames.FOCUS_STOPPED
        }
    },
    setNext: (settings: SettingsState, todayItems: HistoryItem[]) => ({
        type: TimerActionNames.FOCUS_SET_NEXT,
        settings,
        todayItems
    }),
    tick: (now:moment$Moment) => ({
        type: TimerActionNames.FOCUS_TICK,
        now: now
    }),
    none: () => ({
        type: TimerActionNames.NONE
    }),

    nextOccurrence: (completedOccurrences) => ({
        type: TimerActionNames.NEXT_OCCURRENCE,
        completedOccurrences
    }),
    setNotification: () => ({
        type: TimerActionNames.FOCUS_SET_NOTIFICATION
    }),
    unsetNotification: () => ({
        type: TimerActionNames.FOCUS_UNSET_NOTIFICATION
    }),
    foo: () => ({
        type: TimerActionNames.FOO
    }),
    bar: () => ({
        type: TimerActionNames.BAR
    }),
    baz: () => ({
        type: TimerActionNames.BAZ
    })

};


