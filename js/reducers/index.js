import {combineReducers} from 'redux';

import {settingsReducer} from "./settings.reducer";
import {timerReducer as timer} from './timer.reducer';
import {timerEpics} from './timer.actions';
import {historyReducer as history} from './history.reducer'
import {combineEpics} from "redux-observable";
import { reducer as reduxFormReducer } from 'redux-form'
import { NavigationReducer } from '@exponent/ex-navigation';


export const appReducer = combineReducers({
    navigation: NavigationReducer,
    settings: settingsReducer,
    timer,
    history,
    form: reduxFormReducer

});

export const appEpics = combineEpics(...timerEpics)
