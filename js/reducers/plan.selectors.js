// @flow
import {createSelector} from "reselect";
import R from 'ramda';
import moment from 'moment';
import {MomentUtils} from '../utils/moment-utils'
import {OccurrenceType} from '../models';
import {getTodayHistoryItems, getTimer, getNow} from "../selectors";
import type {FocuspolState, TimerComponentSelectedState, TimerState, SettingsState, ExecutionPlanStatistics} from '../models';


export const getTodayExecutionPlanFocusItems = createSelector(
    [getTimer],
    (timer: TimerState) => {
        let isFocusIsToday = item => item.type == OccurrenceType.focus;
        let result = R.filter(isFocusIsToday, timer.executionPlan);
        return result;
    }
)
export const getTodayExecutionPlanItems = createSelector(
    [getTimer],
    (timer: TimerState) => {
        let result = R.clone(timer.executionPlan);
        return result;
    }
)

export const getTodayItems = createSelector(
    [getTodayHistoryItems, getTodayExecutionPlanItems],
    (historyFocusItems, executionPlanFocusItems) => {
        let result = R.concat(historyFocusItems, executionPlanFocusItems);
        return result;
    }
)

export const getCompletedExecutionPlanOccurrences = createSelector(
    state=>state.timer.executionPlan,
    getNow,
    (executionPlan: Occurrence[], now: moment$Moment) => {
        const result = R.filter(occ=> now.isAfter(moment(occ.endDate)))(executionPlan);
        return result;

    }
)