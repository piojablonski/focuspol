// @flow
import {HistoryActionNames} from "./history.actions";
import R from 'ramda';
import moment from 'moment';
import type {Occurrence, HistoryState, HistoryItem} from "../models";
import {GlobalActionNames} from "./global.actions";
import {initialState} from "../initialState";
import {OccurrenceStatus} from "../models/occurrence";

export function historyReducer(state: HistoryState = {}, action: Object = {}): HistoryState {
    switch (action.type) {

        case HistoryActionNames.SAVE_OCCURRENCE:
            if (!action.occurrences || action.occurrences.length == 0)
                return state;


            const occurrences = R.map(occ => R.assoc('status', OccurrenceStatus.finished)(occ), action.occurrences);


            let b = R.assoc('items',
                R.concat(
                    state.items,
                    occurrences),
                state);
            return b;
        case GlobalActionNames.FACTORY_SETTINGS:
            return R.clone(initialState);
        default:
            return state;
    }
}