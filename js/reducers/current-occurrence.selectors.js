// @flow
import type {
    FocuspolState,
    TimerComponentSelectedState,
    TimerState,
    SettingsState,
    ExecutionPlanStatistics
} from '../models';
import {createSelector} from "reselect";
import R from 'ramda';
import moment from 'moment';
import {getTodayExecutionPlanItems} from "../selectors";
import {Occurrence} from "../models/occurrence";
import {getNow} from "./global.selectors";

export const currentOccurrence = createSelector(
    [getTodayExecutionPlanItems],
    (executionPlan: Occurrence[]) => {
        return R.head(executionPlan);
    }
)

export const currentOccurrenceIsCompleted = createSelector(
    [currentOccurrence, getNow],
    (occ: Occurrence, now:moment$Moment) => {
        if (!occ)
            return false;
        return now.isAfter(moment(occ.endDate));
    }
)
export const currentOccurrenceIsExactlyCompleted = createSelector(
    [currentOccurrence, getNow],
    (occ: Occurrence, now:moment$Moment) => {
        if (!occ)
            return false;
        return now.isSame(moment(occ.endDate));
    }
)

