// @flow
import type {SettingsState} from "../models";

export const SettingsActionNames = {
    UPDATE: 'settings/UPDATE',
}

export const settingsActions = {
    update: (newSettings: SettingsState) => ({
        type: SettingsActionNames.UPDATE,
        newSettings,
    })

}