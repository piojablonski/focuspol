// @flow
export const GlobalActionNames = {
    FACTORY_SETTINGS: 'global/FACTORY_SETTINGS',
}

export const globalActions = {
    factorySettings: () => ({
        type: GlobalActionNames.FACTORY_SETTINGS
    })

}