// @flow
import R from 'ramda';
import {REHYDRATE} from "redux-persist/constants";
import moment from 'moment';
import {Action} from "rxjs";
import type {SettingsState} from "../models/index";
import {SettingsActionNames} from "./settings.actions";
import {GlobalActionNames} from "./global.actions";
import {focuspolConfig} from "../config";
import {initialState} from "../initialState";

export const settingsReducer = function(state: SettingsState = {}, action:Action = {}) {
    let newState : SettingsState;

    switch (action.type) {
        case SettingsActionNames.UPDATE:
            newState = R.merge(state, action.newSettings);
            return newState;

        case GlobalActionNames.FACTORY_SETTINGS:
            return R.clone(initialState);
        default:
            return state;
    }
};
