// @flow
import R from 'ramda';
import moment from 'moment';
import {REHYDRATE} from "redux-persist/constants";
import {TimerActionNames, timerActions} from "./timer.actions";
import type {SettingsState, HistoryItem, TimerState, FocuspolState, TimerComponentSelectedState} from '../models';
import {OccurrenceType, Occurrence} from '../models';
import {GlobalActionNames} from "./global.actions";
import {initialState} from "../initialState";

export const timerReducer = function (state?: TimerState = {}, action: Object = {}): TimerState {
    if (!action.type)
        return state;


    let result = R.cond([
        [R.equals(TimerActionNames.FOCUS_PLAY), () => FOCUS_PLAY(state, action)],
        [R.equals(TimerActionNames.FOCUS_TICK), () => FOCUS_TICK(state, action)],
        [R.equals(TimerActionNames.NEXT_OCCURRENCE), () => NEXT_OCCURRENCE(state, action)],
        [R.equals(TimerActionNames.FOCUS_STOPPED), () => FOCUS_STOPPED(state, action)],
        [R.equals(TimerActionNames.FOCUS_SET_NOTIFICATION), () => R.assoc('isNotificationSet', true, state)],
        [R.equals(TimerActionNames.FOCUS_UNSET_NOTIFICATION), () => R.assoc('isNotificationSet', false, state)],
        [R.equals(GlobalActionNames.FACTORY_SETTINGS), () => R.clone(initialState)],
        [R.T, () => state]
    ])(action.type);

    return result;


};

function FOCUS_TICK(state: TimerState, action): TimerState {

    return {
        ...state,
        currentTimerValue : action.now.toJSON()
    }
    // if (!state || !state.executionPlan || state.executionPlan.length == 0)
    //     return state;
    //
    //
    // if (state.executionPlan.length > 0) {
    //     let executionPlan = state.executionPlan;
    //     //tick(state.executionPlan);
    //     let currentOccurrence: Occurrence = R.head(executionPlan);
    //
    //     let newState = R.evolve({
    //             currentTimerValue: () => tick(currentOccurrence).toJSON(),
    //             // executionPlan: () => executionPlan
    //         }
    //     )(state);
    //
    //     return newState;
    // } else {
    //     return state;
    // }
}

// function tick() {
//     return moment().toJSON();
// }

// function tick(currentOccurrence): moment$MomentDuration {
//     let currentTimerValue: moment$MomentDuration;
//     if (currentOccurrence) {
//         currentTimerValue = moment.duration(+moment(currentOccurrence.endDate)).subtract(+moment());
//     } else {
//         currentTimerValue = moment.duration(0);
//     }
//     return currentTimerValue;
// }

function NEXT_OCCURRENCE(state: TimerState, action): TimerState {
    // console.log('NEXT_OCCURRENCE');
    //let executionPlan = dropCurrentFocusFromExecutionPlan(state.executionPlan);
    // console.log('dropCurrentFocusFromExecutionPlan - result', state.executionPlan, executionPlan);

    let completedOccurrences = action.completedOccurrences;

    // todo should drop by occurrence id not by length in lists

    if (!completedOccurrences || completedOccurrences.length == 0)
        return state;

    const executionPlan = R.drop(completedOccurrences.length)(state.executionPlan);


    // if (executionPlan.length == state.executionPlan.length || state.executionPlan.length == 0)
    //     return state;

    let newState = R.evolve({
            executionPlan: () => executionPlan,
            //currentTimerValue: () => tick(),
        }
    )(state)

    return newState;
}

function dropCurrentFocusFromExecutionPlan(executionPlan): Occurrence[] {
    // console.log('dropCurrentFocusFromExecutionPlan', executionPlan);
    let currentOccurrence: Occurrence = R.head(executionPlan);

    if (!currentOccurrence || Occurrence.isCompleted(currentOccurrence) == false) {
        // console.log('[dropCurrentFocusFromExecutionPlan] - not completed');
        return executionPlan;
    } else {
        // console.log('[dropCurrentFocusFromExecutionPlan] - completed - dropping');
        return dropCurrentFocusFromExecutionPlan(R.drop(1, executionPlan));
    }

}

function FOCUS_STOPPED(state: TimerState, action): TimerState {
    return R.pipe(
        R.assoc('isRunning', false)
    )(state);
}

function FOCUS_PLAY(state, action) {
    console.log('FOCUS_PLAY');
    let currentOccurrence: Occurrence = R.head(action.executionPlan);
    // const currentTimerValue = moment.duration(+moment(currentOccurrence.endDate)).subtract(+moment());
    return R.pipe(
        R.assoc('isRunning', true),
        R.assoc('executionPlan', action.executionPlan),
       // R.assoc('currentTimerValue', tick())
    )(state);
}

// function FOCUS_SET_NEXT(state, action) {
//     const {settings, todayItems} : {settings: SettingsState, todayItems: HistoryItem[]} = action;
//     const newCurrentFocus = TimerHelper.getNextOccurrence(settings, todayItems);
//
//     return R.evolve(
//         {
//             occurrence: () => newCurrentFocus,
//             //isNotificationSet: ()=>false
//         }
//     )(state);
// }



