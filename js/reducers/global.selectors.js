// @flow
import type {
    FocuspolState,
    TimerComponentSelectedState,
    TimerState,
    SettingsState,
    ExecutionPlanStatistics
} from '../models';
import {createSelector} from "reselect";
import moment from 'moment';

export const getSettings = (state: FocuspolState) => {
    return state.settings;
}

export const getTimer = (state: FocuspolState) => {
    return state.timer;
}

export const getHistory = (state: FocuspolState) => {
    return state.history;
}

export const getNow = createSelector(
    [getTimer],
    (timer: TimerState) => {
        let now;
        if (!timer.currentTimerValue)
            now = null;
        else {
            now = moment(timer.currentTimerValue);
        }
        return now;
    });