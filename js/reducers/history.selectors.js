// @flow
import {createSelector} from "reselect";
import R from 'ramda';
import moment from 'moment';
import {MomentUtils} from '../utils/moment-utils'
import {OccurrenceType} from '../models';


const getHistoryItems = state=> state.history.items;
export const getTodayHistoryItemsCount = createSelector(
    [getHistoryItems],
    historyItems => {
        let isToday = item => MomentUtils.isToday(item.startDate);
        return R.filter(isToday, historyItems).length;
    }
)

export const getTodayHistoryItems = createSelector(
    [getHistoryItems],
    historyItems => {
        let isToday = item => MomentUtils.isToday(item.startDate);
        return R.filter(isToday, historyItems);
    }
)

export const getTodayHistoryFocusItems = createSelector(
    [getHistoryItems],
    historyItems => {

        let isFocusIsToday = item => MomentUtils.isToday(item.startDate) && item.type == OccurrenceType.focus;
        let result = R.filter(isFocusIsToday, historyItems);
        console.log('getTodayHistoryFocusItems', result);
        return result;
    }
)

export const getLastHistoryItem = createSelector(
    [getHistoryItems],
    historyItems => {
        return R.last(historyItems);
    }
)