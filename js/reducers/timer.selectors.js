// @flow
import {createSelector, } from "reselect";
import R from 'ramda';
import type {FocuspolState, TimerComponentSelectedState, TimerState, SettingsState, ExecutionPlanStatistics} from '../../models';
import  {Occurrence} from '../models';
import {getTodayHistoryFocusItems, getSettings, getTimer, currentOccurrenceIsCompleted, currentOccurrenceIsExactlyCompleted} from "../selectors";


export const getTimerComponentSelectedState = createSelector(
    [getTimer, getTodayHistoryFocusItems, getSettings, currentOccurrenceIsCompleted, currentOccurrenceIsExactlyCompleted],
    (timer : TimerState, todayHistoryFocusItems : Occurrence[], settings : SettingsState, isCompleted, isExactlyCompleted) : TimerComponentSelectedState => {
        const currentOccurrence = R.head(timer.executionPlan);
        // console.log('getTimerComponentSelectedState isCompleted', isCompleted);

        let result:TimerComponentSelectedState = {
            isOccurrenceCompleted : false,
            isPlanCompleted : false,
            shouldCreateNotifications : false,
            shouldClearNotifications : false,
            shouldStartRinging :false,
            shouldDisplay : 'beforeStart'
        };

        if (timer.isRunning && !isExactlyCompleted) {
            result.shouldDisplay = 'timer'
        }

        if (timer.isRunning && isExactlyCompleted) {
            result.shouldDisplay = 'occurrenceCompleted'
        }

        result.isPlanCompleted = todayHistoryFocusItems.length >= settings.target && timer.executionPlan.length == 0;
        if (!timer.isRunning && result.isPlanCompleted) {
            result.shouldDisplay = 'planCompleted';

        }
        result.isOccurrenceCompleted = isCompleted;
        result.shouldPlayAlarm = isExactlyCompleted && timer.isRunning;
        return result;
    }
)

export const getExecutionPlanStatistics = createSelector(
    [getTimer, getTodayHistoryFocusItems, getSettings],
    (timer : TimerState, todayHistoryFocusItems : Occurrence[], settings : SettingsState) : ExecutionPlanStatistics => {


        let focusLeftToday = settings.target - todayHistoryFocusItems.length;
        let result: ExecutionPlanStatistics = {
            focusLeftToday : focusLeftToday > 0 ? focusLeftToday : 0,
            focusFinishedToday : todayHistoryFocusItems.length
        };

        return result;
    }
)


