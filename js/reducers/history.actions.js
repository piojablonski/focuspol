// @flow
import type {Occurrence} from "../models"

export const HistoryActionNames = {
    SAVE_OCCURRENCE: 'SAVE_OCCURRENCE',
}

export const historyActions = {
    saveFocus: (occurrences: Occurrence[]) => ({
        type: HistoryActionNames.SAVE_OCCURRENCE,
        occurrences,
    })
}