// @flow
import moment from 'moment';
import R from 'ramda';


export const MomentUtils = {
    isToday : (date:string) => {
        let today = moment().startOf('day');
        return moment(date).startOf('day').isSame(today);
    },

    formattedDuration : (duration: moment$MomentDuration | string) : string => {
        let d : moment$MomentDuration;
        
        if (typeof duration === 'string')
            d = moment.duration(duration);
        else
            d = duration;

        if (!moment.isDuration(d)) {
            console.error('invalid argument on formattedDuration', duration);
            return "";
        }


        let seconds = d.seconds();
        let minutes = d.minutes();
        let hours = d.hours();
        let text = '';
        if (hours > 0) {
            text += `${hours} hours `;
        }
        if (minutes > 0) {
            text += `${minutes} minutes `;
        }
        if (seconds >= 0) {
            text += `${seconds} seconds`;
        }



        return text;
    },

    formatNDate : (date: string | moment$Moment): string => {
        let m : moment$Moment;
        if (typeof date === 'string') {
            m = moment(date);
        } else {
            m = date;
        }
        return m.format("YYYY-MM-DDTHH:mm:ss.SSSZ");
    },



    getMomentFromString : (m : moment$Moment | string) : moment$Moment => {
        let x: moment$Moment;

        if (typeof m === 'string')
            x = moment(m);
        else
            x = m;

        if (!moment.isMoment(x)) {
            console.error('invalid argument on formattedDuration', m);
            return "";
        }
        return x;
    },

    formattedTime : (m : moment$Moment) : string =>{
        return m.format('HH:mm:ss');
    },

    formattedShortTime : (m : moment$Moment|string) : string =>{
        return MomentUtils.getMomentFromString(m).format("HH:mm:ss");
    },




}



