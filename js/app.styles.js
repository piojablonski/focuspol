// @flow
import {StyleSheet} from "react-native";
import NativeTachyons from 'react-native-style-tachyons';

//header blue 84	130	214
// background color 240	239	244



export const fontSizes = {
    buttonTitle : 44,
    title : 20,
    body : 17,
    small : 12


}

export const appColors = {
    headerBlue : 'rgb(84,130,214)',

    borderGray: 'rgb(177, 180, 182)',
    lightGray:'#F9FAFB',
    darkGray : '#E5E5E5',
    white : 'white',
    navyblue: '#33333D',
    navyblue1: '#585898',
    navyblue2: '#89899F',
    navyblue3: '#B0B0C1',
    champagne1: '#A9911A',
    champagne2: '#C5B988',

    textColor: 'black',

    success: '#C5B358'
};


export const appStyles = StyleSheet.create({
    rootContainer : {
    },
    rootContent : {
        backgroundColor: 'rgb(240,239,244)',
        flex:1
    },

    whiteText : {
        color : 'white'
    },

    headerContainer : {
        backgroundColor : appColors.champagne2,
    },
    headerTitleContainer : {
        flexDirection: 'column',
        justifyContent: 'space-around'
    },
    headerTitleText : {
        color:appColors.lightGray,

        textAlign: 'center',

    },

    footerContainer: {
        backgroundColor: 'rgb(233, 237,237)',
        borderTopColor: appColors.borderGray,
        borderTopWidth: 1
    },

    footerButton : {
        borderRadius:0,
        //color:'tomato'
        backgroundColor:'transparent',
        height:54


    },
    footerIconActive: {
        color:appColors.champagne1
    },

    footerIcon : {
        borderRadius:0,
        color:appColors.navyblue2,
        fontSize:28
    }


});
