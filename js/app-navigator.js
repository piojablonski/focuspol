// @flow
import React, {Component} from 'react';
import {statusBarColor} from './themes/base-theme';
import {View} from 'react-native';
import {AlarmSound} from './components/alarm-sound/alarm-sound.component';

import {
    NavigationProvider,
    StackNavigation,
    TabNavigation,
    TabNavigationItem as TabItem,
} from '@exponent/ex-navigation';

import {Router} from "./router";

export class AppNavigator extends Component {


    render() {
        return (
            <View style={{flex:1}} >
                <TabNavigation
                    id="main"
                    navigatorUID="main"
                    initialTab="plan">
                    <TabItem
                        id="timer"
                        title="timer">
                        <StackNavigation
                            id="home"
                            navigatorUID="home"
                            initialRoute={Router.getRoute('timer')}


                        />
                    </TabItem>
                    <TabItem
                        id="plan"
                        title="plan">
                        <StackNavigation
                            id="planList"
                            navigatorUID="planList"
                            initialRoute={Router.getRoute('planList')}
                        />
                    </TabItem>
                    <TabItem
                        id="settings"
                        title="settings">
                        <StackNavigation
                            id="posts"
                            initialRoute={Router.getRoute('settings')}
                        />
                    </TabItem>
                </TabNavigation>
                <AlarmSound />

            </View>

        );
    }

}



