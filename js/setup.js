// @flow
import React, {Component} from 'react';
import {Provider} from 'react-redux';
import {Text, StyleSheet, PushNotificationIOS, AppState} from 'react-native';
import type {SettingsState, HistoryItem, TimerState, FocuspolState, TimerComponentSelectedState} from '../models';
import configureStore from './configureStore';
import {timerActions} from "./reducers/timer.actions";
import {AppNavigator} from './app-navigator';


import {
    NavigationProvider,
    NavigationContext
} from '@exponent/ex-navigation';

import {Router} from "./router";
import NativeTachyons, {styles as s} from "react-native-style-tachyons";
import {appColors} from "./app.styles";

NativeTachyons.build({
    colors: {
        palette: {
            ...appColors
        }
    }


}, StyleSheet);

if (__DEV__) {
    console.log('Development');
} else {
    console.log('Production');
}

export class Root extends Component {
    navigationContext: any;

    constructor() {
        super();
        this.state = {
            isLoading: true,
            store: configureStore(
                () => {
                    console.log('setup rehydrate complete');
                    this.setState({isLoading: false});


                }
            ),
        };

        this.navigationContext = new NavigationContext({
            router: Router,
            store: this.state.store,
        })
    }

    // componentDidMount = () => {
    //     // console.log('componentDidMount');
    //     AppState.addEventListener('change', currentAppState => {
    //         this.appState = currentAppState;
    //     });
    // }

    componentWillMount() {



        PushNotificationIOS.checkPermissions((permissions) => {
            console.log('checking permission PushNotificationIOS', permissions);
            const permissionsToAskFor = {
                alert: true,
                badge: true,
                sound: true
            };
            if (!permissions.sound || !permissions.alert || !permissions.badge) {
                PushNotificationIOS.requestPermissions(permissionsToAskFor)
            }
        });
    }


    componentDidUpdate() {

        let state: FocuspolState = this.state.store.getState();
        let shouldResume = state.timer.isRunning;
        console.log('setup component did update', shouldResume);
        //if (shouldResume) {
            this.state.store.dispatch(timerActions.resume());
        //}


    }

    render() {
        return (
            <Provider store={this.state.store}>
                {
                    this.state.isLoading ?
                        <Text>loading</Text> :
                        (
                            <NavigationProvider context={this.navigationContext}>
                                <AppNavigator  />
                            </NavigationProvider>
                        )
                }
            </Provider>
        );
    }
}


