// @flow
import {StyleSheet} from "react-native";

export const formStyles = StyleSheet.create({
    form: {
        flex: 0,
        flexDirection: 'column',
        paddingLeft: 16,
        paddingRight: 16,
    },
    text: {
        marginTop: 10,
        height: 20
    },
    textfield: {
        height: 28,  // have to do it on iOS
        width: 100,
        backgroundColor: 'white',
        marginTop: 8
    },
    picker: {
        width: 100,
        flex: 0
    },
    pickerItem: {
        color: 'tomato',
        fontSize: 10,

    },
    buttonsContainer: {
        flex: 0,
        flexDirection: 'row'

    },
    submitbutton: {

    }
    ,
});