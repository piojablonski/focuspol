// @flow
import React, {Component} from 'react';
import {BackAndroid, StatusBar, NavigationExperimental, View, Text, TouchableHighlight} from 'react-native';
import {connect} from 'react-redux';
import {appStyles} from "../../app.styles";
import NativeTachyons, {styles as s} from "react-native-style-tachyons";
import {PJButton} from "../common/button.component";
import {
    isDirty,
    isValid,
    isPristine,
    isSubmitting,
    hasSubmitSucceeded,
    submit,
    getFormValues
} from 'redux-form'
import bindActionCreators from "redux/src/bindActionCreators";

@connect(state => ({
        valid: isValid('settingsForm')(state),
        pristine: isPristine('settingsForm')(state),
    }),
    dispatch => ({
        dispatch,
        actions: bindActionCreators({submit}, dispatch)
    })
)
export class SettingsHeaderComponent extends Component {

    handleSubmit = ()=>{
        this.props.actions.submit('settingsForm');
    }
    render() {
        //console.log('SettingsHeaderComponent render', this.props)
        const {pristine, valid} = this.props;
        const isSaveDisabled = pristine || !valid;
        return (
            <View style={[s.flx_i, s.jcc]}>
                <PJButton onPress={this.handleSubmit} disabled={isSaveDisabled} label="save">
                </PJButton>
            </View>
        )
    }
}