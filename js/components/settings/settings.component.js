// @flow
import React from 'react';
import {connect} from "react-redux";
import {View, ScrollView, Switch, Text, TextInput, TouchableHighlight, Picker, Slider} from 'react-native';
import {Col, Row, Grid} from "react-native-easy-grid";
import {StyleSheet} from "react-native";
import {appColors} from "../../app.styles";
import Rx from 'rxjs';
import 'rxjs';
import {settingsActions} from "../../reducers/settings.actions";
import type {SettingsState} from "../../models";
import NativeTachyons, {styles as s} from "react-native-style-tachyons";
import {PJButton} from "../common/button.component";


import moment from 'moment';
import bindActionCreators from "redux/src/bindActionCreators";
import {SettingsForm} from "./settings-form.component";
import {SettingsHeaderComponent} from "./settings-header.component";
import {globalActions} from "../../reducers/global.actions";


const mapDispatchToProps = (dispatch) => ({
    dispatch,
    settingsActions: bindActionCreators(settingsActions, dispatch),
    globalActions: bindActionCreators(globalActions, dispatch),
});


@connect(store => ({
    settings: store.settings
}), mapDispatchToProps)
export class SettingsComponent extends React.Component {

    static route = {
        navigationBar: {
            title: 'ustawienia',
            backgroundColor: appColors.white,
            tintColor : appColors.navyblue,
            borderBottomColor: appColors.borderGray,
            renderRight: ()=> <SettingsHeaderComponent/>

        }

    };

    props: {
        settings : SettingsState,
        dispatch: (action: any) => void,
        settingsActions : any,
        globalActions : any

    };

    componentWillUnmount() {
        console.log('SettingsComponent ComponentWillUnmount');
    }

    onSubmit = (formValues: any) => {
        console.log('form submitted', formValues);
        this.props.settingsActions.update(formValues);
    };

    resetAll = () => {
        const {globalActions} = this.props;
        this.props.globalActions.factorySettings();

    }

    render() {
        let {settings} = this.props;
        console.log('SettingsComponent render()');


        return (
            <ScrollView style={[s.ph3, s.pv1]}>
                <SettingsForm
                    onSubmit={this.onSubmit}/>
                <PJButton label="reset all" onPress={this.resetAll}></PJButton>
            </ScrollView>
        )

    }

}
