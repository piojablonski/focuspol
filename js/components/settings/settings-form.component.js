// @flow
import React from "react";
import {connect} from "react-redux";
import {Switch, View, TextInput, Text, Slider, Picker} from "react-native";
import {Field, reduxForm} from 'redux-form';
import R from "ramda";
import moment from "moment";
import {formStyles} from "./settings-form.styles";
import {appColors} from "../../app.styles";
import {styles as s} from "react-native-style-tachyons";
import {focuspolConfig} from "../../config";

@connect(store => ({initialValues: store.settings}))
@reduxForm({
    form: 'settingsForm',
    enableReinitialize: true
})
export class SettingsForm extends React.Component {
    render() {
        const {handleSubmit, reset, pristine, dirty, valid} = this.props;
        return (
            <View style={[s.pb5]}>
                <Text style={formStyles.text}>username:</Text>
                <Field name="username" style={formStyles.textfield} component={Input}
                       validate={this.isRequired}></Field>
                <Text style={formStyles.text}>target:</Text>
                <FieldNumberSlider name="target" step={1} minimumValue={0} maximumValue={16}/>
                <Text style={formStyles.text}>long break after:</Text>
                <FieldNumberSlider name="longBreakAfter" step={1} minimumValue={0} maximumValue={16}/>
                <Text style={formStyles.text}>work duration:</Text>
                <FieldDurationSlider name="workDuration" step={1} minimumValue={1} maximumValue={30}/>
                <Text style={formStyles.text}>short break duration:</Text>
                <FieldDurationSlider name="shortBreakDuration" step={1} minimumValue={1} maximumValue={30}/>
                <Text style={formStyles.text}>long break duration:</Text>
                <FieldDurationSlider name="longBreakDuration" step={1} minimumValue={1} maximumValue={30}/>
                <Text style={formStyles.text}>autoplay:</Text>
                <Field name="autoplay" component={fieldSwitch}></Field>
            </View>
        )

    }

    isRequired = (value: any, allValues: any) => {
        return !R.isEmpty(value) ? undefined : 'please fill your name';
    }

    componentWillMount() {
        this.props.onSubmitSuccess = () => {
            this.props.reset();
        }
    }
}

function fieldSwitch(props) {
    const {input} = props;
    return (
        <Switch
            onValueChange={input.onChange}
            value={input.value}
        />
    )
}

function Input({input, style, meta : {error, warning, dirty}}) {

    return (
        <View style={{flexDirection:'column', flex:1}}>
            {/*<Text>{touched?'true':'false'} {error} {dirty?'true':'false'}</Text>*/}
            <TextInput
                onChangeText={input.onChange}
                value={input.value}
                style={style}
            />
            {dirty && ((error && <Text>{error}</Text>) || (warning && <Text>{warning}</Text>))}
        </View>

    );
}

function targetPicker(props) {


    const {input, style, itemStyle} = props;
    console.log('targetPicker', props);
    const options = R.range(0, 16);

    return (
        <Picker
            selectedValue={input.value}
            onValueChange={input.onChange}
            style={style}
            itemStyle={itemStyle}
        >
            {
                options.map(v => (
                    <Picker.Item label={v.toString()} value={v} key={v}/>
                ))
            }
        </Picker>
    );


}

function SliderView(props) {
    let {input : {value, onChange}} = props;
    return (
        <View>
            <Text>{value}</Text>
            <Slider
                {...props}
                style={[]}
                value={value}
                onValueChange={onChange}
                minimumTrackTintColor={appColors.champagne2}

            />
        </View>
    );
}

function FieldNumberSlider(props) {
    return (
        <Field
            {...props}
            component={SliderView}
            format={(value, name)=>{
                const res = R.defaultTo(0)(parseInt(value));
                return res;
            }}

        />

    );
}

function FieldDurationSlider(props) {
    return (
        <Field
            {...props}
            component={SliderView}
            format={(value, name)=>{
                let res = moment.duration(value).as(focuspolConfig.baseMomentUnit);
                return res;
            }}
            parse={(value, name)=>{
                return moment.duration(value, focuspolConfig.baseMomentUnit).toJSON();
            }}

        />

    );
}
