import {StyleSheet} from "react-native";
import {appColors, fontSizes} from '../../app.styles';

export const timerStyles = StyleSheet.create({
    title: {
        fontSize: 19,
        fontWeight: 'bold',
    },
    activeTitle: {
        color: 'red',
    },

    timerContainer: {
        backgroundColor : appColors.lightGray,
        justifyContent:'center',
        alignItems : 'center',
        flex:1,
    },
    timerTextCounter : {
        color: appColors.champagne1,
        fontSize : fontSizes.title
    },
    timerTextSecondary : {
        color: appColors.champagne2,
        fontSize : fontSizes.body
    },
    timerTextStopped : {
        color: appColors.champagne1,
        fontSize : fontSizes.body
    },
    controlContainer : {
        flex:2,
        flexDirection:'row',

        alignItems:'center',
        justifyContent:'center',
        backgroundColor: appColors.white
    },
    controlButton : {
        width:128,
        height:128,
        borderRadius:64,
        backgroundColor:appColors.navyblue2,
        justifyContent : 'center',
        alignItems: 'center'

    },
    controlButtonText : {
        color:appColors.lightGray,
        fontSize: fontSizes.buttonTitle / 2

    }
});
