// @flow
import React from 'react';
import {Text, Button, View, TouchableHighlight} from 'react-native';
import {connect} from "react-redux";
import {timerActions} from "../../reducers/timer.actions";
import {getTodayHistoryFocusItems, getTodayHistoryItems} from "../../reducers/history.selectors";
import type {
    HistoryItem,
    SettingsState,
    FocuspolState,
    TimerState,
    TimerComponentSelectedState,
    ExecutionPlanStatistics
} from "../../models";
import {historyActions} from "../../reducers/history.actions";
import R from 'ramda';
import {MomentUtils} from '../../utils/moment-utils';


import {TimerHelper} from "../../business/timer-helper";
import {soundService} from "../../business/sound-service";
import NativeTachyons, {styles as s} from "react-native-style-tachyons";
import {timerStyles} from "./timer.styles";
import {getTimerComponentSelectedState, getExecutionPlanStatistics} from "../../reducers/timer.selectors";
import bindActionCreators from "redux/src/bindActionCreators";
import {appColors} from "../../app.styles";
import moment from 'moment';
import {Occurrence} from "../../models/occurrence";
import {getNow} from "../../reducers/global.selectors";


type FocusComponentProps = {
    timer : TimerState,
    settings : SettingsState,
    todayFocusHistoryItems : HistoryItem[],
    todayHistoryItems : HistoryItem[],
    selectedState : TimerComponentSelectedState,
    statistics:ExecutionPlanStatistics,
    dispatch: (action: any)=>void,
    timerActions : any,
    historyActions : any,
    now : moment$Moment
}
const mapStateToProps = (state: FocuspolState) => {
    return {
        timer: state.timer,
        settings: state.settings,
        todayFocusHistoryItems: getTodayHistoryFocusItems(state),
        todayHistoryItems: getTodayHistoryItems(state),
        selectedState: getTimerComponentSelectedState(state),
        statistics: getExecutionPlanStatistics(state),
        now : getNow(state)
    }
};

const mapDispatchToProps = (dispatch) => ({
    dispatch,
    timerActions: bindActionCreators(timerActions, dispatch),
    historyActions: bindActionCreators(historyActions, dispatch)
});

@connect(mapStateToProps, mapDispatchToProps, null, {pure: true})
export class FocusComponent extends React.Component {

    props: FocusComponentProps;

    static route = {
        navigationBar: {
            title: 'timer',
            backgroundColor: appColors.white,
            tintColor: appColors.navyblue,
            borderBottomColor: appColors.borderGray
        }
    }

    componentWillMount() {

    };

    componentDidUpdate(prevProps: any, prevState: any) {
        // if (timer.isRunning && !timer.isNotificationSet) {
        //     if (settings.autoplay)
        //         LocalNotificationsManager.createAllLocalNotifications(executionPlan);
        //     else
        //         LocalNotificationsManager.createNextNotification(executionPlan);
        //
        //     this.props.dispatch(currentFocusActions.setNotification());
        // }


        // if (this.compState.isExecutionPlanCompleted) {
        //     console.log('execution plan completed');
        //     this.props.dispatch(currentFocusActions.stop());
        // }

    };

    onClickStart = () => {
        this.handleStart();
    };

    handleStart = () => {
        const {settings, todayHistoryItems, timerActions, timer:{executionPlan}, now} = this.props;
        timerActions.play(settings, todayHistoryItems, executionPlan, now);
    };


    onClickStop = () => {
        this.handleStop();
    };

    handleStop = () => {
        const {timerActions} = this.props;
        timerActions.stop();
    };

    renderTimer() {
        const {timer} = this.props;

        //let elapsedText = MomentUtils.formattedDuration(timer.currentTimerValue);

        let currentOccurrence: Occurrence = R.head(timer.executionPlan);
        let elapsedText = MomentUtils.formattedDuration(
            moment.duration(moment(currentOccurrence.endDate).diff(timer.currentTimerValue))
        );
        // let elapsedText =
        //     //moment.duration(
        //         moment(currentOccurrence.endDate).diff(timer.currentTimerValue, 'ms');

        //console.log(elapsedText);

        if (!currentOccurrence)
            return null;
        return (
            <View style={timerStyles.timerContainer}>
                <Text style={timerStyles.timerTextCounter}>{elapsedText}</Text>
                <Text style={timerStyles.timerTextSecondary}>{currentOccurrence.type}</Text>
            </View>
        )
    };

    renderBeforeStarted() {
        const {settings} = this.props;
        return (
            <View style={timerStyles.timerContainer}>
                <Text style={timerStyles.timerTextStopped}>Hi {settings.username}
                    you have not started yet, click 'start' to start timer</Text>
            </View>
        );
    };

    renderOccurrenceCompleted() {
        return (
            <View style={timerStyles.timerContainer}>
                <Text style={timerStyles.timerTextStopped}>pomodoro complete!</Text>
            </View>
        );
    };

    renderPlanComplete() {
        return (
            <View style={timerStyles.timerContainer}>
                <Text style={timerStyles.timerTextStopped}>work complete!</Text>
            </View>
        );
    };

    renderButton() {
        const {isRunning} = this.props.timer;
        return isRunning ?
            <TouchableHighlight style={timerStyles.controlButton} onPress={this.onClickStop}>
                <Text style={timerStyles.controlButtonText}>stop</Text>
            </TouchableHighlight>
            :
            <TouchableHighlight style={timerStyles.controlButton} onPress={this.onClickStart}>
                <Text style={timerStyles.controlButtonText}>start</Text>
            </TouchableHighlight>
    }

    render() {
        const {selectedState, timer} = this.props;

        let clock: any;
        switch (selectedState.shouldDisplay) {
            case "timer":
                clock = this.renderTimer();
                break;
            case "beforeStart":
                clock = this.renderBeforeStarted();
                break;
            case "occurrenceCompleted":
                clock = this.renderOccurrenceCompleted();
                break;
            case "planCompleted":
                clock = this.renderPlanComplete();
                break;
        }

        return (
            <View style={[s.flx_i, s.ph3, s.pv1, s.pb3]}>
                {clock}
                <View style={timerStyles.controlContainer}>
                    {this.renderButton()}
                </View>
                <HistoryStatus statistics={this.props.statistics}></HistoryStatus>
            </View>

        )
    };
}

function HistoryStatus({statistics}) {
    return (
        <View style={[s.p5, s.flx_row, s.flx_wrap, s.jcsb]}>
            <StatisticsText>completed today {statistics.focusFinishedToday}</StatisticsText>
            <StatisticsText>left today {statistics.focusLeftToday}</StatisticsText>
        </View>
    )
}

function StatisticsText(props) {
    return <Text {...props} style={[s.f6, s.h1, {flex:0} ]}>{props.children}</Text>
}
