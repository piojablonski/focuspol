// @flow
import {Text, TouchableHighlight} from "react-native";
import NativeTachyons, {styles as s} from "react-native-style-tachyons";
import React from "react";
import {appColors} from "../../app.styles";
import R from 'ramda';


export class PJButton extends React.Component {


    render() {
        const props = this.props;
        const {disabled, label, style} = props;

        const isDisabled = disabled && disabled == true;

        const baseButtonStyle = R.defaultTo([])(style).concat([s.pa2]);
        const baseTextStyle = [s.f5]
        let buttonStyle, textStyle;
        buttonStyle = baseButtonStyle;
        textStyle = baseTextStyle;
        if (!isDisabled) {
            buttonStyle = baseButtonStyle.concat([s.b__textColor]);
            textStyle = baseTextStyle.concat([s.textColor]);
        }

        else if (isDisabled) {
            buttonStyle = baseButtonStyle.concat([s.b__darkGray]);
            textStyle = baseTextStyle.concat([s.darkGray]);
        }

        return (
            <TouchableHighlight {...props} style={buttonStyle} underlayColor={appColors.champagne1}>
                <Text style={textStyle}>{label}</Text>
            </TouchableHighlight>
        )


    }
}

