// @flow
import React from 'react';
import {connect} from "react-redux";
import 'rxjs';
import bindActionCreators from "redux/src/bindActionCreators";
import {ListView, Row, Heading, Text, Caption, View, Subtitle, Divider} from "@shoutem/ui"
import Icon from 'react-native-vector-icons/EvilIcons';
import {getTodayItems, currentOccurrence} from "../../selectors";
import type {
    FocuspolState,
    TimerComponentSelectedState,
    TimerState,
    SettingsState,
    ExecutionPlanStatistics
} from '../../models';
import {Occurrence, OccurrenceStatus, OccurrenceType} from "../../models/occurrence";
import {MomentUtils} from "../../utils/moment-utils";

import {appColors} from "../../app.styles";
import NativeTachyons, {styles as s} from "react-native-style-tachyons";
import {focuspolConfig} from "../../config";
import moment from 'moment';


const mapDispatchToProps = (dispatch) => ({
    dispatch,
    // settingsActions: bindActionCreators(settingsActions, dispatch),
});


@connect(store => ({
    occurrences: getTodayItems(store),
    settings: store.settings,
    currentOccurrence: currentOccurrence(store)

}), mapDispatchToProps)
export class PlanListComponent extends React.Component {

    render() {

        const {occurrences, settings, currentOccurrence} = this.props;
       // console.log('currentOccurrence', currentOccurrence);


        return (<ListView
            data={occurrences}
            renderRow={(data, o)=>OccurrenceRow(data, currentOccurrence, settings)}
        />);
    }

}

function OccurrenceRow(occurrence: Occurrence, currentOccurrence: Occurrence, settings: SettingsState) {
    const caption = `${MomentUtils.formattedShortTime(occurrence.startDate)}-${MomentUtils.formattedShortTime(occurrence.endDate)}`;

    const isCurrentOccurrence = currentOccurrence && occurrence.id && occurrence.id === currentOccurrence.id;



    if (occurrence.type == OccurrenceType.focus) {
        return occurrence.status == OccurrenceStatus.queue ? QueueFocusRow(occurrence, isCurrentOccurrence, settings) : HistoryFocusRow(occurrence, settings);
    } else {
        return BreakRow(occurrence, isCurrentOccurrence, settings);
    }
}

function QueueFocusRow(occurrence: Occurrence, isCurrentOccurrence: boolean, settings: SettingsState) {
    const caption = `${MomentUtils.formattedShortTime(occurrence.startDate)}-${MomentUtils.formattedShortTime(occurrence.endDate)}`
    return (
        <Row>
            <EiIcon name={isCurrentOccurrence?'spinner-2':'clock'}/>
            <View styleName="vertical">
                <View styleName="horizontal space-between">
                    <Subtitle styleName="">{occurrence.index}</Subtitle>
                    <Caption>{caption}</Caption>
                </View>
            </View>
        </Row>);
}

function BreakRow(occurrence: Occurrence, isCurrentOccurrence: boolean, settings: SettingsState) {
    const caption = `${MomentUtils.formattedShortTime(occurrence.startDate)}-${MomentUtils.formattedShortTime(occurrence.endDate)}`

    const length = Occurrence.getDuration(occurrence).asMilliseconds();
    const shortBreakMiliseconds = moment.duration(settings.shortBreakDuration).asMilliseconds();
    const longBreakMiliseconds = moment.duration(settings.longBreakDuration).asMilliseconds();

    let dividerHeight = 100;

    if (length < longBreakMiliseconds)
        dividerHeight = 10;
    if (length >= longBreakMiliseconds)
        dividerHeight = 50;

    return (
        <Divider styleName="section-header" style={{height:dividerHeight, backgroundColor:isCurrentOccurrence?appColors.champagne1:appColors.borderGray}}></Divider>
    );
}


function HistoryFocusRow(occurrence: Occurrence, settings: SettingsState) {
    const caption = `${MomentUtils.formattedShortTime(occurrence.startDate)}-${MomentUtils.formattedShortTime(occurrence.endDate)}`
    return (
        <Row>
            <EiIcon name="check" style={[s.success]}/>
            <View styleName="vertical">
                <View styleName="horizontal space-between">
                    <Subtitle styleName="">{occurrence.index}</Subtitle>
                    <Caption>{caption}</Caption>
                </View>
            </View>
        </Row>);
}

function EiIcon(props) {
    return (<Icon  {...props} style={[props.style, {fontSize:20, marginRight:8}]}/>)
}
