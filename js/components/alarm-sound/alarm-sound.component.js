// @flow
import React from 'react';
import {connect} from "react-redux";
import type {
    HistoryItem,
    SettingsState,
    FocuspolState,
    TimerState,
    TimerComponentSelectedState,
    ExecutionPlanStatistics
} from "../../models";
import {soundService} from "../../business/sound-service";
import {getTimerComponentSelectedState, getExecutionPlanStatistics} from "../../reducers/timer.selectors";
import bindActionCreators from "redux/src/bindActionCreators";


const mapStateToProps = (state: FocuspolState) => {
    return {
        selectedState: getTimerComponentSelectedState(state),
    }
};

// const mapDispatchToProps = (dispatch) => ({
//     dispatch,
//     timerActions: bindActionCreators(timerActions, dispatch),
//     historyActions: bindActionCreators(historyActions, dispatch)
// });

@connect(mapStateToProps/*, mapDispatchToProps*/)
export class AlarmSound extends React.Component {

    render() {
        const {selectedState} = this.props;
        if (selectedState.shouldPlayAlarm) {
            soundService.playFocusAlarm();
        }
        return null;
    }

}