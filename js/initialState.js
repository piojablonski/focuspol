// @flow
import type {FocuspolState, SettingsState, HistoryState, TimerState} from "./models";
import moment from 'moment';
import {focuspolConfig} from "./config";
import {fakeOccurrence} from "../testUtils/utils";
import {OccurrenceType, OccurrenceStatus} from "./models/occurrence";

const defaultSettingsInitialState: SettingsState = {
    username: 'Piotr initial',
    workDuration: moment.duration(25, focuspolConfig.baseMomentUnit).toJSON(),
    shortBreakDuration: moment.duration(5, focuspolConfig.baseMomentUnit).toJSON(),
    longBreakDuration: moment.duration(15, focuspolConfig.baseMomentUnit).toJSON(),
    longBreakAfter: 4,
    target: 12,
    autoplay: true
};

const testSettingsInitialState: SettingsState = {
    username: 'Piotr initial',
    workDuration: moment.duration(5, 's').toJSON(),
    shortBreakDuration: moment.duration(5, 's').toJSON(),
    longBreakDuration: moment.duration(15, 's').toJSON(),
    longBreakAfter: 4,
    target: 12,
    autoplay: true
};

const defaultTimerInitialState: TimerState = {
    isRunning: false,
    isNotificationSet: false,
    currentTimerValue: "",
    executionPlan: []
};

const defaultHistoryInitialState: HistoryState = {
    items: []
}

const testHistoryInitialState: HistoryState = {
    items: [
        {...fakeOccurrence(-120, undefined, 's'), status: OccurrenceStatus.finished, index: 1},
        {...fakeOccurrence(-95, OccurrenceType.shortBreak, 's'), status: OccurrenceStatus.finished},
        {...fakeOccurrence(-90, undefined, 's'), status: OccurrenceStatus.finished, index: 2},
        {...fakeOccurrence(-75, OccurrenceType.shortBreak, 's'), status: OccurrenceStatus.finished},
    ]
}


export const initialState: FocuspolState = {
    settings: testSettingsInitialState,
    history: testHistoryInitialState,
    timer: defaultTimerInitialState
}