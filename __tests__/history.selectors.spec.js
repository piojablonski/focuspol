// @flow
import {getLastHistoryItem, getTodayHistoryItemsCount, getTodayHistoryItems} from '../js/reducers/history.selectors.js';
import moment from 'moment';
import chai, {expect} from 'chai';
import {OccurrenceType} from "../js/models";
import {fakeHistoryItem} from '../testUtils/utils';
import R from 'ramda';

xdescribe('history selectors', ()=>{

    let mockState = {
        history : {
            items : [
                fakeHistoryItem(moment.duration(2, 'd').asMinutes(), 'focus'),
                fakeHistoryItem(moment.duration(3, 'd').asMinutes(), 'focus'),
                fakeHistoryItem(120, 'focus'),
                fakeHistoryItem(90, 'shortBreak'),
                fakeHistoryItem(85, 'focus'),
            ]
        }
    }

    it('should get last item from list', ()=>{
        let actual = getLastHistoryItem(mockState);
        let expected = R.last(mockState.history.items);
        expect(actual).to.deep.equal(expected);
    });

    it('should get list of today history items', () => {

        let actual = getTodayHistoryItems(mockState);
        let expected = R.takeLast(3, mockState.history.items);
        expect(actual).to.be.deep.equal(expected);
    });






});