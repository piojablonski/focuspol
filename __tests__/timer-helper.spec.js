// @flow weak

import chai, {expect} from 'chai';

import moment from 'moment';
import R from 'ramda';
import {fakeHistoryItem} from "../testUtils/utils";
import type {SettingsState, TimerState} from "../js/models";
import {OccurrenceType, Occurrence} from "../js/models";
import {TimerHelper} from '../js/business/timer-helper';


xdescribe('TimerHelper', ()=>{
    const settings: SettingsState = {
        workDuration: moment.duration(25, 'm').toJSON(),
        shortBreakDuration: moment.duration(5, 'm').toJSON(),
        longBreakDuration: moment.duration(15, 'm').toJSON(),
        longBreakAfter: 4,
        target: 12,
        autoplay: true
    };

    const currentOccurrence: Occurrence = {
        isRunning: false,
        occurrence: Occurrence.createEmptyOccurrence(),
        isNotificationSet: false
    }

    describe('getNextOccurrenceType', ()=>{

        it(`should return 'focus' when first occurrence today`, () => {
            let actual = TimerHelper.getNextOccurrenceType(settings, []);
            expect(actual).to.equal(OccurrenceType.focus);
        });




    });

    describe('getNextOccurrence', () => {
        it(`should return occurrence with endDate bigger by 25 minutes then startdate`, ()=>{
            let actual = TimerHelper.generateNextOccurrence(settings, []);
            expect(actual.endDate).to.equal(moment(actual.startDate).add(25, 'm').toJSON());
        })
    })

    describe('getExecutionPlan', ()=>{

        it(`should return 2 focuses and 1 shortBreak`, () => {
            const mSettings = R.assoc('target', 2, settings);
            let res = TimerHelper.createExecutionPlan(mSettings, []);
            let expected = [OccurrenceType.focus, OccurrenceType.shortBreak, OccurrenceType.focus];
            let actual = R.map(o=>o.type, res);
            expect(actual).to.eql(expected);
        });

        it(`should return 4 focuses and 4 shortBreaks, 1 long break`, () => {
            const mSettings = R.assoc('target', 5, settings);
            let res = TimerHelper.createExecutionPlan(mSettings, []);
            let expected = [
                OccurrenceType.focus,
                OccurrenceType.shortBreak,
                OccurrenceType.focus,
                OccurrenceType.shortBreak,
                OccurrenceType.focus,
                OccurrenceType.shortBreak,
                OccurrenceType.focus,
                OccurrenceType.longBreak,
                OccurrenceType.focus,
            ];
            let actual = R.map(o=>o.type, res);
            expect(actual).to.eql(expected);
        });

        it(`should return 2 focuses and 1 shortBreaks, 1 long break`, () => {
            const mSettings = R.assoc('target', 5, settings);
            const todaysItems = [
                fakeHistoryItem(85, OccurrenceType.focus),
                fakeHistoryItem(60, OccurrenceType.shortBreak),
                fakeHistoryItem(55, OccurrenceType.focus),
                fakeHistoryItem(30, OccurrenceType.shortBreak),
                fakeHistoryItem(25, OccurrenceType.focus),
            ]

            let res = TimerHelper.createExecutionPlan(mSettings, todaysItems);
            let expected = [
                OccurrenceType.shortBreak,
                OccurrenceType.focus,
                OccurrenceType.longBreak,
                OccurrenceType.focus,
            ];
            let actual = R.map(o=>o.type, res);
            expect(actual).to.eql(expected);
        });

        it('should create 5 actions with consecutive start date', () => {
            const mSettings = R.assoc('target', 5, settings);
            let res = TimerHelper.createExecutionPlan(mSettings, []);
            const startDate = moment(res[0].startDate);
           // console.log(R.pluck('startDate', res));
            expect(res[1].startDate).to.equal(moment(res[0].startDate).add(25, 'm').toJSON());
            expect(res[2].startDate).to.equal(moment(res[1].startDate).add(5, 'm').toJSON());
            expect(res[3].startDate).to.equal(moment(res[2].startDate).add(25, 'm').toJSON());
            expect(res[4].startDate).to.equal(moment(res[3].startDate).add(5, 'm').toJSON());
            expect(res[5].startDate).to.equal(moment(res[4].startDate).add(25, 'm').toJSON());
            expect(res[6].startDate).to.equal(moment(res[5].startDate).add(5, 'm').toJSON());
            expect(res[7].startDate).to.equal(moment(res[6].startDate).add(25, 'm').toJSON());
            expect(res[8].startDate).to.equal(moment(res[7].startDate).add(15, 'm').toJSON());



        });


    });

});
