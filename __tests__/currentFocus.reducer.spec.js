// @flow weak

// import {timerReducer} from '../js/reducers/currentFocus.reducer';
// import chai, {expect, should} from 'chai';
//
// import moment from 'moment';
// import R from 'ramda';
// import {CurrentFocusActionNames, currentFocusActions} from "../js/reducers/currentFocus.actions";
// import {fakeHistoryItem} from "../testUtils/utils";
// import type {SettingsState, TimerState} from "../js/models";
// import {OccurrenceType, Occurrence} from "../js/models";
// should();

// describe('currentFocusReducer', () => {
//
//     const settings: SettingsState = {
//         workDuration: moment.duration(25, 'm').toJSON(),
//         shortBreakDuration: moment.duration(5, 'm').toJSON(),
//         longBreakDuration: moment.duration(15, 'm').toJSON(),
//         longBreakAfter: 4,
//         target: 12,
//         autoplay: true
//     };
//
//     const emptyState: TimerState = {
//         isRunning: false,
//        // occurrence: Occurrence.createCurrentFocus(),
//         isNotificationSet: false,
//         currentTimerValue : moment.duration(0).toJSON(),
//         executionPlan:[]
//     }
//
//     it('empty action should return state', () => {
//         let state = R.assoc('foo', 'foo', emptyState);
//         let actual = timerReducer(state, {});
//         expect(actual).to.deep.equal(state);
//         // chai.expect(actual).to.deep.equal(state);
//     });
//
//     it('unknown action should return state', () => {
//         let state = R.assoc('foo', 'foo', emptyState);
//         let actual = timerReducer(state, {type: 'whatever_blablabla'});
//         expect(actual).to.deep.equal(state);
//         // chai.expect(actual).to.deep.equal(state);
//     });
//
//     xdescribe('FOCUS_SET_NEXT', () => {
//
//
//         const setNextAction = {
//             type: CurrentFocusActionNames.FOCUS_SET_NEXT,
//             settings,
//             todayItems: null
//         }
//
//
//         xit('deprecated should set isNotificationSet to false', () => {
//
//             const state = R.evolve({
//                 isNotificationSet : () => true
//             })
//
//             let actualState = timerReducer(state(emptyState), setNextAction);
//
//             expect(actualState.isNotificationSet).to.equal(false);
//         });
//
//         it(`should return 'focus' when there is no history item`, () => {
//             let actualState = timerReducer(emptyState, setNextAction);
//             let actual = actualState;
//             expect(actualState.occurrence.type).to.equal(OccurrenceType.focus);
//
//         });
//         it(`should be FocusType.focus when last item was a shortbreak`, () => {
//             let action = R.pipe(
//                 R.assoc('todayItems', [fakeHistoryItem(30, 'shortBreak')])
//             )(setNextAction);
//             let actual = timerReducer(emptyState, action);
//             expect(actual.occurrence.type).to.be.equal(OccurrenceType.focus);
//
//         });
//
//         it('should be focus when last item was a longBreak', () => {
//
//             let action = R.pipe(
//                 R.assoc('todayItems', [fakeHistoryItem(30, 'longBreak')])
//             )(setNextAction);
//             let actual = timerReducer(emptyState, action);
//             expect(actual.occurrence.type).to.be.equal(OccurrenceType.focus);
//
//
//         });
//
//         it('should be longBreak after 4 focus', () => {
//
//             let action = R.pipe(
//                 R.assoc('todayItems', [
//                     fakeHistoryItem(30, OccurrenceType.focus),
//                     fakeHistoryItem(30, OccurrenceType.shortBreak),
//                     fakeHistoryItem(30, OccurrenceType.focus),
//                     fakeHistoryItem(30, OccurrenceType.shortBreak),
//                     fakeHistoryItem(30, OccurrenceType.focus),
//                     fakeHistoryItem(30, OccurrenceType.shortBreak),
//                     fakeHistoryItem(30, OccurrenceType.focus)
//                 ])
//             )(setNextAction);
//             let actual = timerReducer(emptyState, action);
//             expect(actual.occurrence.type).to.be.equal('longBreak');
//
//
//         });
//
//         it('should be longBreak after 3 focus', () => {
//
//             let action = R.pipe(
//                 R.assoc('todayItems', [
//                     fakeHistoryItem(30, OccurrenceType.focus),
//                     fakeHistoryItem(30, OccurrenceType.longBreak),
//                     fakeHistoryItem(30, OccurrenceType.focus),
//                     fakeHistoryItem(30, OccurrenceType.shortBreak),
//                     fakeHistoryItem(30, OccurrenceType.focus),
//                     fakeHistoryItem(30, OccurrenceType.shortBreak),
//                     fakeHistoryItem(30, OccurrenceType.focus)
//                 ]),
//                 R.assocPath(['settings', 'longBreakAfter'], 3)
//             )(setNextAction);
//
//             let actual = timerReducer(emptyState, action);
//             expect(actual.occurrence.type).to.be.equal('longBreak');
//
//
//         });
//
//         it('should be shortBreak after 2 focus', () => {
//             let action = R.pipe(
//                 R.assoc('todayItems', [
//                     fakeHistoryItem(30, OccurrenceType.focus),
//                     fakeHistoryItem(30, OccurrenceType.shortBreak),
//                     fakeHistoryItem(30, OccurrenceType.focus)
//                 ])
//             )(setNextAction);
//             let actual = timerReducer(emptyState, action);
//             expect(actual.occurrence.type).to.be.equal('shortBreak');
//         });
//
//         it(`should set expected, current duration for 'focus'`, () => {
//             let action = setNextAction;
//             let actual = timerReducer(emptyState, action);
//
//             expect(actual.occurrence.expectedDuration).to.be.equal(moment.duration(25, 'm').toJSON());
//             expect(actual.occurrence.currentDuration).to.be.equal(moment.duration(0).toJSON());
//         });
//
//         it(`should set expected, current duration for 'shortBreak'`, () => {
//             let action = R.pipe(
//                 R.assoc('todayItems', [
//                     fakeHistoryItem(30, OccurrenceType.focus),
//                 ])
//             )(setNextAction);
//             let actual = timerReducer(emptyState, action);
//
//             expect(actual.occurrence.expectedDuration).to.be.equal(moment.duration(5, 'm').toJSON());
//             expect(actual.occurrence.currentDuration).to.be.equal(moment.duration(0).toJSON());
//         });
//
//         it(`should set expected, current duration for 'longBreak'`, () => {
//             let action = R.pipe(
//                 R.assoc('todayItems', [
//                     fakeHistoryItem(30, OccurrenceType.focus),
//                 ]),
//                 R.assocPath(['settings', 'longBreakAfter'], 1)
//             )(setNextAction);
//             let actual = timerReducer(emptyState, action);
//
//             expect(actual.occurrence.expectedDuration).to.be.equal(moment.duration(15, 'm').toJSON());
//             expect(actual.occurrence.currentDuration).to.be.equal(moment.duration(0).toJSON());
//         });
//
//
//     });
//
//     describe('FOCUS_PLAY', () => {
//         it('should set startdate and isRunning', () => {
//             let actual = timerReducer(emptyState, currentFocusActions.play());
//
//             expect(actual.isRunning).to.be.equal(true);
//
//
//             expect(actual.occurrence.startDate).to.exist;
//
//         });
//     });
//
//     describe('FOCUS_STOP', () => {
//         it('should set isRunning', () => {
//             let actual = timerReducer(emptyState, currentFocusActions.stop());
//             expect(actual.isRunning).to.be.equal(false);
//         });
//     });
//
//     describe('FOCUS_SET_NOTIFICATION', () => {
//         it('should set notification', () => {
//             let actual = timerReducer(emptyState, currentFocusActions.setNotification());
//             expect(actual.isNotificationSet).to.be.equal(true);
//         });
//     });
// });

