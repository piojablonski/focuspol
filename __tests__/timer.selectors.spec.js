// @flow
import moment from 'moment';
import chai, {expect} from 'chai';
import {OccurrenceType} from "../js/models";
import type {
    TimerState,
    FocuspolState,
    TimerComponentSelectedState,
    SettingsState,
    HistoryState
} from '../js/models/index';
import {getTimerComponentSelectedState} from '../js/reducers/timer.selectors';
import {fakeOccurrence, fakeHistoryItem} from "../testUtils/utils";

import R from 'ramda';

fdescribe('timer component selectors', () => {

    const mockSettingsState: SettingsState = {
        workDuration: moment.duration(25, 'm').toJSON(),
        shortBreakDuration: moment.duration(5, 'm').toJSON(),
        longBreakDuration: moment.duration(15, 'm').toJSON(),
        longBreakAfter: 4,
        target: 4,
        autoplay: true
    };

    let mockExecutionPlan = () => ([
        fakeOccurrence(),
        fakeOccurrence(25, OccurrenceType.shortBreak),
        fakeOccurrence(30, OccurrenceType.focus),
        fakeOccurrence(55, OccurrenceType.shortBreak),
        fakeOccurrence(60, OccurrenceType.focus),
        fakeOccurrence(85, OccurrenceType.shortBreak),
        fakeOccurrence(90, OccurrenceType.focus),
    ]);


    const mockState : FocuspolState = {
        settings: mockSettingsState,
        history: {
            items: []
        },
        timer: {
            isRunning: false,
            isNotificationSet: false,
            currentTimerValue: '',
            executionPlan: []
        }

    };


    it('should show beforeStart when clock is not running and executionplan is empty', () => {

        const mockTimerState: TimerState = {
            isRunning: false,
            isNotificationSet: false,
            currentTimerValue: '',
            executionPlan: []
        }
        let actual = getTimerComponentSelectedState(
            {
                ...mockState,
                timer: mockTimerState
            });
        let expected: TimerComponentSelectedState = {
            isOccurrenceCompleted: false,
            isPlanCompleted: false,
            shouldCreateNotifications: false,
            shouldClearNotifications: false,
            ShouldStartRinging: false,
            ShouldStopRinging: false,
            shouldDisplay: 'beforeStart'
        }
        expect(actual).to.deep.equal(expected);
    });

    it('should show timer when clock is running', () => {

        const mockTimerState: TimerState = {
            isRunning: true,
            isNotificationSet: true,
            currentTimerValue: '',
            executionPlan: R.clone(mockExecutionPlan())
        }
        let actual = getTimerComponentSelectedState({
            ...mockState,
            timer: mockTimerState
        });
        expect(actual.shouldDisplay).to.equal('timer');
    });

    it('should show occurrence completed when first occurrence in execution plan is completed', () => {

        const mockTimerState: TimerState = {
            isRunning: true,
            isNotificationSet: true,
            currentTimerValue: '',
            executionPlan: [
                fakeOccurrence(-26, OccurrenceType.focus),
                fakeOccurrence(-1, OccurrenceType.shortBreak),
            ]
        }
        let actual = getTimerComponentSelectedState(
            {
                ...mockState,
                timer: mockTimerState
            });
        expect(actual.shouldDisplay).to.equal('occurrenceCompleted');
    });

    it('should show plan completed when execution plan is empty, isRunning is false and there is settings.target number of completed `focus` in history items', () => {

        const mockTimerState: TimerState = {
            isRunning: false,
            isNotificationSet: true,
            currentTimerValue: '',
            executionPlan: []
        }

        const mockHistoryState: HistoryState = {
            items: [
                fakeHistoryItem(120),
                fakeHistoryItem(90),
                fakeHistoryItem(60),
                fakeHistoryItem(30),
            ]
        }


        let actual = getTimerComponentSelectedState(
            {
                ...mockState,
                timer: mockTimerState,
                history: mockHistoryState
            });
        expect(actual.shouldDisplay).to.equal('planCompleted');
    });


});