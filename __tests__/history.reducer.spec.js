// @flow
import {historyReducer} from '../js/reducers/history.reducer';
import {historyActions} from '../js/reducers/history.actions';
import type {HistoryState, Occurrence, HistoryItem} from '../js/models'

import moment from 'moment';
import R from 'ramda';
import {fakeHistoryItem, fakeOccurrence} from "../testUtils/utils";

import chai from 'chai';
const {expect, should} = chai;


xdescribe('historyReducer', ()=>{

    let historyInitialState : HistoryState = {
        items: []
    }

    it('should push HistoryItem to state', ()=>{
        let cFocus : Occurrence = fakeOccurrence(30);
        let actual = historyReducer(historyInitialState, historyActions.saveFocus(cFocus, moment().toJSON()));

        //let expected = R.assoc('items', R.append([], cFocus))(historyInitialState);
        expect(actual.items.length).to.equal(1);

    });

    it('should push HistoryItem to state as last item', ()=>{
        const state = R.assoc('items',
            R.concat(
                R.prop('items', historyInitialState),
                [fakeHistoryItem(90), fakeHistoryItem(60)]
            ),
            historyInitialState);

        let cFocus : Occurrence = fakeOccurrence(30);
        let newState = historyReducer(historyInitialState, historyActions.saveFocus(cFocus, moment().toJSON()));
        let actual : HistoryItem = R.last(newState.items);

        expect(actual.startDate).to.equal(cFocus.startDate);
    });
});

