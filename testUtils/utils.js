// @flow
import moment from 'moment';
import type {HistoryItem, Occurrence} from '../js/models';
import {OccurrenceType} from '../js/models';

export const fakeHistoryItem = (startElapsedInMinutes:number, type? : string = 'focus') : HistoryItem => {
    let duration;
    if (type == 'focus')
        duration = 25;
    else if (type == 'shortBreak')
        duration = 5;
    else
        duration = 15;



    let result : HistoryItem = {
        startDate: moment().subtract(startElapsedInMinutes, 'm').toJSON(),
        endDate: moment().subtract(startElapsedInMinutes - 30, 'm').toJSON(),
        expectedDuration: moment.duration(duration).toJSON(),
        type: type
    }
    return result;
};

export const fakeOccurrence = (startInMinutes:number = 0, type? : string = OccurrenceType.focus, momentUnit:string = 'm') : Occurrence => {
    let duration;
    if (type == 'focus')
        duration = 25;
    else if (type == 'shortBreak')
        duration = 5;
    else
        duration = 15;



    let result : Occurrence = {
        startDate: moment().add(startInMinutes, momentUnit).toJSON(),
        endDate:  moment().add(startInMinutes + duration, momentUnit).toJSON(),
        type: type,

    }
    return result;
};