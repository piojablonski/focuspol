
import { AppRegistry } from 'react-native';
import {Root} from './js/setup';
import "babel-polyfill";

AppRegistry.registerComponent('focuspol', ()=>Root);
